<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class notification extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> load -> helper('url');
		$this -> load -> helper('html');
		$this -> load -> database();
		$this -> load -> library('Native_Session');
		$this -> load -> library('Session');
		$this -> load -> library('Message_stack');
		$this -> load -> library('email');
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");

		//load all models
		$this -> load -> model("comman_model", "comman", true);
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("users_model", "user", true);
	}

	function index()
	{
		$this->admin->isLogin();
		$this->load->view("admin/push_notification");
	}

	function add_post()
	{	
		$this->admin->isLogin();
		$class = "danger";
		$request_data = $this->comman_lib->get_data();
		if(empty($request_data)){
			$url = base_url() . 'notification';
			redirect($url);
		}

		//Image Upload
		if ($_FILES['Image']['name'] != '') {
			$path = APPPATH.'upload/';
			$allowedTypes = 'gif|jpg|png|jpeg';
			$fieldname = 'Image';
			$folderName = "notification";
			$result = $this->comman_lib->uploadPhoto($fieldname, $path, $allowedTypes, $_FILES,$folderName,true,512,true);
			if (!empty($result)) {
				$request_data['image_path'] = base_url().'application/upload/'.$folderName."/".$result;
				$request_data['thumb_path'] = base_url().'application/upload/'.$folderName."/thumb_small/".$result;
			}
		}
		//End image upload
		if($this->sendGCM($request_data)){
			$message = "Notification send successfully.".json_encode($request_data);
			$class = "success";
		}else{
			unlink($request_data['image_path']);
			unlink($request_data['thumb_path']);
			$message = "Notification error occure.";
		}

		$this -> message_stack -> add_message('message', $message);
		$this -> message_stack -> add_message('class', $class);
		$url = base_url() . 'notification';
		redirect($url);
	}

	private function sendGCM($data)
	{
		// prepare options array, you can use custom key s
		$opts_array = array(
		    //'message'   => json_encode($data),
		    'message'   => $data['message'],
		    'title'     => $data['title'],
		    'subtitle'  => 'Sample test message',
		    'tickerText'    => 'Piyush Patel',
		    'vibrate'   => $data['vibrate'],
		    'sound'     => $data['sound'],
		    'largeIcon' => $data['image_path'],
		    'smallIcon' => $data['thumb_path']
		);
		
		// load gcm library
		$this->load->library('gcm_notification');
		
		// place your recipients here. select gcm_id from db or smth. don't use $key=>$value
		$users = $this->user->get_all();
		if(!empty($users))
		{
			foreach ($users as $key => $value) {
				if(!empty($value['gcm_device_id']))
				{
					$reg_ids[] = $value['gcm_device_id'];
				}
			}
		}else{
			$reg_ids[] = 'f9eTt7sEkxM:APA91bFoUhacOgRXLdWA4CFsssxc3erqMfppuRPwGlpvVqt2Mn1akR647csy6v9on_hPmS3VnPEJI0goOPvNaly6Yb8ctp3y2CvsYvIhan9lpAN2gEx0A4r2OBN836vCDyUHXy2cnMYU';
		}
		//echo "<pre>";print_r($reg_ids);exit;
		// seting recipient
		$this->gcm_notification->setRecipients($reg_ids);

		/* set Time To Live - How long (in seconds) the message should be kept on GCM storage if the device is offline.
		 Optional (default time-to-live is 4 weeks, and must be set as a JSON number).*/
		$this->gcm_notification->setTTL(500);

		// set collapse Key
		/*An arbitrary string (such as "Updates Available") that is used to collapse a group of like messages when the device is
		 offline, so that only the last message gets sent to the client. This is intended to avoid sending too many messages to
		 the phone when it comes back online.*/
		$this->gcm_notification->setCollapseKey('GCM_Library');

		// takes boolean
		/*If included, indicates that the message should not be sent immediately if the device is idle. The server will wait for
		the device to become active, and then only the last message for each collapse_key value will be sent.*/
		$this->gcm_notification->setDelay(true);

		// set predefined options
		$this->gcm_notification->setOptions($opts_array);

		// debug info. http_headers if (400,401,500) and success if 200. takes boolean
		$this->gcm_notification->setDebug(true);

		// finally send it. if DEBUG is TRUE , print , returns array
		$result = $this->gcm_notification->send();
		if(isset($result) && !empty($result))
		{
			if($result['success'] == 1 && $result['message'] == 'success')
			{
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

}