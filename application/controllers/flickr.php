<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class flickr extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> load -> helper('url');
		$this -> load -> helper('html');
		$this -> load -> database();
		$this -> load -> library('Native_Session');
		$this -> load -> library('Session');
		$this -> load -> library('Message_stack');
		$this -> load -> library('email');
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		
		//load all models
		$this -> load -> model("comman_model", "comman", true);
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("pictures_model", "pictures", true);
		$this -> load -> model("category_model", "category", true);
	}

	function index()
	{
        $url = base_url() . 'flickr/add';
        redirect($url);
	}
    
	function add()
	{
		$this->admin->isLogin();
        $data["copyrights"] = $this->pictures->get_copyright();
        $data["categories"] = $this->pictures->get_category();
        $this -> load -> view("admin/add_flickr",$data);
	}
	
	function add_post()
	{	
		$this->admin->isLogin();
		$class = "danger";
		$request_data = $this->comman_lib->get_data();
        //echo "<pre>";print_r($request_data);exit;
        if(!$this->pictures->get_by_source_link($request_data['photo_source_link'])){
            $this -> message_stack -> add_message('message', $request_data['image_name']." Flickr Picture already exists in database");
            $this -> message_stack -> add_message('class', $class);
            $url = base_url() . 'flickr/add';
            redirect($url);
        }
		$this->load->library('form_validation');
		$this->form_validation->set_rules('image_name', 'Name', 'required');
		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('tags', 'Tag', 'required');
		$this->form_validation->set_rules('photo_source_link', 'Photo sorce link', 'required|edit_unique[tbl_picture.photo_source_link.'.$request_data["id"].']');
		if ($this->form_validation->run() == FALSE)
		{
			$message = validation_errors();
			$this -> message_stack -> add_message('message', $message);
			$this -> message_stack -> add_message('class', $class);
            $url = base_url() . 'flickr/add';
			redirect($url);
		}else{
			if (!empty($request_data['category_id'])) {
				$request_data['category_id'] = implode($request_data['category_id'], ',');
			}
			
			$tags = explode(',',$request_data['tags']);
			
			$tag_ids = array();
			foreach($tags as $tag){
				$tagId = $this->pictures->check_tags($tag);
				
				if($tagId > 0){
					array_push($tag_ids,$tagId);
				}else{
					$tagId = $this->pictures->add_tags($tag);
					array_push($tag_ids,$tagId);
				}	
			}
			
			$request_data['tags'] = implode(',',$tag_ids);
			$request_data['tags'] = rtrim($request_data['tags'],",");

			$request_data['upload_date'] = date('Y-m-d');
			
			if(empty($request_data)){
				$url = base_url() . 'flickr';
				redirect($url);
			}
			
			//Image Upload
            $content = file_get_contents($request_data['image_path']);
            //Store in the filesystem.
            $image_name = time().".jpg";
            $fp = fopen(APPPATH."upload/picture/".$image_name, "w");
            fwrite($fp, $content);
            fclose($fp);
            $path = APPPATH.'upload/';
            $folderName = "picture";
            $isUpload = $this->comman_lib->socialImage($image_name,$path,$folderName,true,512,true);
            if($isUpload)
            {
                $request_data['image_path'] = 'application/upload/'.$folderName."/".$image_name;
                $request_data['thumb_path'] = 'application/upload/'.$folderName."/thumb_small/".$image_name;
                $request_data['360_path'] = 'application/upload/'.$folderName."/thumb_360/".$image_name;
                $request_data['720_path'] = 'application/upload/'.$folderName."/thumb_720/".$image_name;

                list($request_data['image_width'],$request_data['image_height']) = getimagesize(base_url().$request_data['image_path']);
                list($request_data['thumb_width'],$request_data['thumb_height']) = getimagesize(base_url().$request_data['thumb_path']);
                list($request_data['360_width'],$request_data['360_height']) = getimagesize(base_url().$request_data['360_path']);
                list($request_data['720_width'],$request_data['720_height']) = getimagesize(base_url().$request_data['720_path']);
            }
			//End image upload
			
			unset($request_data["image_id"]);
            //echo "<pre>";print_r($request_data);exit;
            $request_data['is_show'] = 'y';
			if($request_data["id"] != ""){
				if($this->pictures->update($request_data)){
					$message = "Flickr Picture update successfully.";
					$class = "success";
				}else{
					$message = "Internal error occure.";
				}
			}else{
				$id = $this->pictures->insert($request_data);
				if(!empty($id)){
					$message = "Flickr Picture added successfully.";
					$class = "success";
				}else{
					$message = "Internal error occure.";
				}
			}
			$this -> message_stack -> add_message('message', $message);
			$this -> message_stack -> add_message('class', $class);
			$url = base_url() . 'flickr';
			redirect($url);
		}
	}
}