<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this -> load -> helper('url');
		$this -> load -> helper('html');
		$this -> load -> database();
		$this -> load -> library('Native_Session');
		$this -> load -> library('Session');
		$this -> load -> library('Message_stack');
		$this -> load -> library('email');
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		//load all models
		$this -> load -> model("comman_model", "comman", true);
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("users_model", "user", true);
        $this -> load -> model("pictures_model", "pictures", true);
	}

	function index() {
		if ($this -> session -> userdata('AdminID')) {
			$url = base_url() . 'category';
			redirect($url);
		}

		$this -> load -> view("admin/login");
	}

	function dashboard() {
		$this -> admin -> isLogin();
		$url = base_url() . 'category';
		redirect($url);
		//$this -> load -> view("admin/dashboard");
	}
	
	function dashboard_add()
	{
		$this->admin->isLogin();
		$request_data = $this->comman_lib->get_data();
		if($request_data["DashboardID"] != "")
		{
			$data["record"] = $this->dashboard->get_by_id($request_data["DashboardID"]);
		}
		$this->load->view("admin/add_dashboard_image",$data);
	}
	
	function dashboard_add_post()
	{
		$this->admin->isLogin();
		$class = "danger";
		$request_data = $this->comman_lib->get_data();
		if(empty($request_data)){
			$url = base_url() . 'admin/dashboard';
			redirect($url);
		}
		
		//Image Upload
		if ($_FILES['Image']['error'] == 0) {
			$oldFileName = $request_data['Photo'];
			$path = APPPATH.'upload/';
			$allowedTypes = 'gif|jpg|png|jpeg';
			$fieldname = 'Image';
			$folderName = "DefaultImage";
			$result = $this->comman_lib->uploadPhoto($fieldname, $path, $allowedTypes, $_FILES,$folderName,true);
			if (!empty($result)) {
				$request_data['Photo'] = $result;
				unlink($path.$folderName."/".$oldFileName);
			}
		}
		//End image upload
		
		if($request_data["DashboardID"] != ""){
			if($this->dashboard->update($request_data)){
				$message = "Dashboard image update successfully.";
				$class = "success";
			}else{
				$message = "Internal error occure.";
			}
		}else{
			$id = $this->dashboard->insert($request_data);
			if(!empty($id)){
				$message = "Dashboard image added successfully.";
				$class = "success";
			}else{
				$message = "Internal error occure.";
			}
		}
		$this -> message_stack -> add_message('message', $message);
		$this -> message_stack -> add_message('class', $class);
		$url = base_url() . 'admin/dashboard';
		redirect($url);
	}

	function dashboard_delete()
	{
		$class = "danger";
		$this->admin->isLogin();
		$request_data = $this->comman_lib->get_data();
		if(empty($request_data)){
			$url = base_url() . 'admin/dashboard';
			redirect($url);
		}
		$request_data["IsDelete"] = "1";
		if($this->dashboard->update($request_data)){
			$message = "Dashboard image delete successfully.";
			$class = "success";
		}else{
			$message = "Internal error occure.";
		}
		$this -> message_stack -> add_message('message', $message);
		$this -> message_stack -> add_message('class', $class);
		$url = base_url() . 'admin/dashboard';
		redirect($url);
	}
	
	function setting() {
		$this -> admin -> isLogin();
		$AdminId = $this -> session -> userdata('AdminID');
		$data = $this -> input -> post();
		if (!empty($data)) {
			$message = "Internal error occured.";
			$class = "danger";
			if($this -> admin -> update($data))
			{
				$this->session->set_userdata("Name",$data['Name']);
				$message = 'Setting updated successfully.';
				$class = 'success';
			}
			$this->message_stack->add_message('message', $message);
			$this->message_stack->add_message('class', $class);
			$url = base_url() . 'admin/setting';
			redirect($url);
		}

		$setting_data['admin'] = $this -> admin -> get_data_by_id($AdminId);
		$this -> load -> view("admin/setting",$setting_data);
	}

	function login() {
		$email = $this -> input -> post('Email');
		$password = $this -> input -> post('Password');
		if ($email != "" && $password != "") {
			$admin = $this -> admin -> authenticate($email, $password);
			if ($admin['Status'] == '0') {
				$this -> message_stack -> add_message('message', 'Your account is not active, Please contact Administrator to active your account.');
				$this -> message_stack -> add_message('class', 'danger');
				$url = base_url() . 'admin';
				redirect($url);
			} elseif (!empty($admin)) {
				$update['LastLoginAt'] = date("Y-m-d H:i:s",time());
				$update['AdminID'] = $admin['AdminID'];
				$this->admin->update($update);
				// print_r($admin);exit;
				$this -> session -> set_userdata($admin);
				$url = base_url() . 'admin/dashboard';
				redirect($url);
			} else {
				$this -> message_stack -> add_message('message', 'Invalid email address or password.');
				$this -> message_stack -> add_message('class', 'danger');
				$url = base_url() . 'admin';
				redirect($url);
			}
		} else {
			$url = base_url() . 'admin';
			redirect($url);
		}
	}

	function forgotPassword() {
		if ($this -> session -> userdata('UserID')) {
			$url = base_url() . 'admin/dashboard';
			redirect($url);
		}
		$this -> load -> view("admin/forgot_password");
	}

	function resetPassword() {
		//$name = $this -> input -> post("Name");
		$email = $this -> input -> post("Email");
		if ($email != "") {
			$userData = $this -> admin -> getUserByEmail($email);
			if (!empty($userData)) {
				$data['URL'] = base_url() . 'admin/forgot_user_password?ResetCode=' . base64_encode($email);
				$data['Name'] = $userData['0']['FirstName']." ".$userData['0']['LastName'];
				if($this -> send_mail($data))
				{
					$message = "We have sent a password reset link to the email address you entered. Follow the instructions in the email to reset your password.";
					$class = "success";
				}else{
					$message = "We could not send mail an account associated with {$email} address.";
					$class = "warning";
				}
			} else {
				$message = "We could not find an account associated with {$email} address.";
				$class = "warning";
			}
			$this -> message_stack -> add_message('message', $message);
			$this -> message_stack -> add_message('class', $class);
			$url = base_url() . 'admin/forgotPassword';
			redirect($url);

		} else {
			$url = base_url() . 'admin/forgotPassword';
			redirect($url);
		}
	}

	function forgot_user_password() {
		$data = $this -> comman_lib -> get_data();
		$email = base64_decode($data['ResetCode']);
		$userData["User"] = $this -> admin -> getUserByEmail($email);
		if (!empty($userData)) {
			$this -> load -> view('admin/reset_password', $userData);
		} else {
			$url = base_url() . 'admin/forgotPassword';
			redirect($url);
		}
	}

	function updatePassword() {
		$data = $this -> comman_lib -> get_data();
		if (!empty($data) AND $data["Password"] != "") {
			$data["Password"] = sha1($data["Password"]);
			unset($data["confPassword"]);
			if ($this -> admin -> update($data)) {
				$message = "Your password has been reset successfully.";
				$class = "success";
			} else {
				$message = "Internal error occure.";
				$class = "danger";
			}
		}
		$this -> message_stack -> add_message('message', $message);
		$this -> message_stack -> add_message('class', $class);
		$url = base_url() . 'admin';
		redirect($url);
	}

	protected function send_mail($data) {
		$to = $data['Email'];
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html;charset=iso-8859-1' . "\r\n";
		$headers .= "From: kiosklead.com <" . ADMIN_EMAIL_ID . ">\r\n";
		$subject = "Kiosklead Password Reset";
		$mail_data['data'] = $data;
		$message = $this -> load -> view('admin/forgot_password_template', $mail_data, TRUE);
		$this -> email -> clear();
		//$config['protocol'] = 'smtp';
		//$config['smtp_host'] = 'ssl://smtp.gmail.com';
		//$config['smtp_user'] = 'rtesting97@gmail.com';
		//$config['smtp_pass'] = 'appGuruz12#';
		//$config['smtp_port'] = 465; 
		//$config['smtp_crypto'] = 'ssl';
		$config['wrapchars'] = 255;
    	$config['mailtype'] = "html";
    	$this -> email -> initialize($config);
		$this -> email -> set_newline("\r\n");
		$this -> email -> from($headers);
		$this -> email -> to($to);
		$this -> email -> subject($subject);
		$this -> email -> message($message);
		/*if(mail($to, $subject, $message, $headers))
		{
			return true;
		}else{
			echo $this->phpmailer->ErrorInfo;
		}*/
		if($this -> email -> send($to, $subject, $message, $headers)){
			return true;
		}else{
			echo $this->email->print_debugger();
		}
	}

	/**
	 * Function for Profile
	 * @access public
	 * @return void()
	 * @author manish
	 */
	function profile() {
		$this -> admin -> isLogin();
		$AdminId = $this -> session -> userdata('AdminID');
		$data = $this -> input -> post();
		if (!empty($data)) {
			if(isset($data['oldPassword'])){
				$data['AdminID'] = $AdminId;
				if($this -> admin ->authenticateOldPassword($data)){
					unset($data['oldPassword']);
					$data['Password'] = $this->encrypt->sha1($data['Password']);
					$this -> admin -> update($data);
					$this->message_stack->add_message('message', "Password changed successfully");
					$this->message_stack->add_message('class', "success");
				} else {
					$this->message_stack->add_message('message', "Your current password did not match");
					$this->message_stack->add_message('class', "danger");	
				}
				$url = base_url() . 'admin/profile';
				redirect($url);
			}
			
			$message = "Internal error occured.";
			$class = "danger";
			if($this -> admin -> update($data))
			{
				$this->session->set_userdata("Name",$data['Name']);
				$message = 'Profile updated successfully.';
				$class = 'success';
			}
			$this->message_stack->add_message('message', $message);
			$this->message_stack->add_message('class', $class);
			$url = base_url() . 'admin/profile';
			redirect($url);
		}

		$profile_data['admin'] = $this -> admin -> get_data_by_id($AdminId);
		$this -> load -> view("admin/profile", $profile_data);
	}

	function sign_out() {
		$this -> admin -> isLogin();
		$array_items = array('AdminID' => '', 'Email' => '', 'Name' => '');
		$this -> session -> unset_userdata($array_items);
		$this -> message_stack -> add_message('message', 'Log Out Successfully ');
		$this -> message_stack -> add_message('class', 'success');

		$url = base_url() . 'admin';
		redirect($url);
	}

    function pixabay() {
        $this -> admin -> isLogin();
        $this -> load -> view("admin/pixabay");
    }

    function flickr() {
        $this -> admin -> isLogin();
        $data["copyrights"] = $this->pictures->get_copyright();
        $data["categories"] = $this->pictures->get_category();
        $this -> load -> view("admin/flickr",$data);
    }

    function flickr_submit() {
        $this -> admin -> isLogin();
        $request_data = $this -> input -> post();
        $content = file_get_contents($request_data['image_path']);
        //Store in the filesystem.
        $fp = fopen(APPPATH."upload/".$request_data['image_id'].".jpg", "w");
        fwrite($fp, $content);
        fclose($fp);
        echo "<pre>";print_r($request_data);exit;
    }
}
?>