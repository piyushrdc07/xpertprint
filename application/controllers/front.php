<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Front extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this -> load -> helper('url');
		$this -> load -> helper('html');
		$this -> load -> database();
		$this -> load -> library('Native_Session');
		$this -> load -> library('Session');
		$this -> load -> library('Message_stack');
		$this -> load -> library('email');
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		//load all models
		$this -> load -> model("comman_model", "comman", true);
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("users_model", "user", true);
        $this -> load -> model("pictures_model", "pictures", true);
	}

	function index() {
		$this -> load -> view("public/index");
	}
}
?>