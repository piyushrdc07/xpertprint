<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class picture_detail extends REST_Controller
{
	
	function __construct() {
		parent::__construct();
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		//load all models
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("category_model", "category", true);
		$this -> load -> model("pictures_model", "pictures", true);
		$this -> load -> model("tags_model", "tags", true);
		$this -> load -> model("today_download_model", "today_download", true);
		
		$headers = $this->input->request_headers();
		$this->comman_lib->valid_hash($headers["Hashkey"]); //HashKey Checking
	}

	function get_all_download_post()
	{
	
		$data = $this->comman_lib->get_offset_limit($this->get_data());
		$result = $this->pictures->get_by_download($data);
		$pages = $this->comman_lib->get_no_of_pages($result['pages']);
		$result = $result['result'];
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				if(!empty($value['categories']))
				{
					$result[$key]['categories'] = $this->category->get_by_ids(explode(',', $value['categories']));
				}
				if(!empty($value['tags']))
				{
					$result[$key]['tags'] = $this->tags->get_by_ids(explode(',', $value['tags']));
				}
			}
		}
		if(empty($result)){
			$response['status'] ="success";
			$response['message'] ="No record found";
			$response['pages'] = 1;
		}else{
			$response['status'] ="success";
			$response['recents'] = $result;
			$response['pages'] = $pages;
		}
		
		$this->response($response);	
	}

	function get_week_download_post()
	{
	
		$data = $this->comman_lib->get_offset_limit($this->get_data());
		$result = $this->pictures->get_week_download($data);
		$pages = $this->comman_lib->get_no_of_pages($result['pages']);
		$result = $result['result'];
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				if(!empty($value['categories']))
				{
					$result[$key]['categories'] = $this->category->get_by_ids(explode(',', $value['categories']));
				}
				if(!empty($value['tags']))
				{
					$result[$key]['tags'] = $this->tags->get_by_ids(explode(',', $value['tags']));
				}
			}
		}
		if(empty($result)){
			$response['status'] ="success";
			$response['message'] ="No record found";
			$response['pages'] = 1;
		}else{
			$response['status'] ="success";
			$response['recents'] = $result;
			$response['pages'] = $pages;
		}
		
		$this->response($response);	
	}

	function get_month_download_post()
	{
	
		$data = $this->comman_lib->get_offset_limit($this->get_data());
		$result = $this->pictures->get_month_download($data);
		$pages = $this->comman_lib->get_no_of_pages($result['pages']);
		$result = $result['result'];
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				if(!empty($value['categories']))
				{
					$result[$key]['categories'] = $this->category->get_by_ids(explode(',', $value['categories']));
				}
				if(!empty($value['tags']))
				{
					$result[$key]['tags'] = $this->tags->get_by_ids(explode(',', $value['tags']));
				}
			}
		}
		if(empty($result)){
			$response['status'] ="success";
			$response['message'] ="No record found";
			$response['pages'] = 1;
		}else{
			$response['status'] ="success";
			$response['recents'] = $result;
			$response['pages'] = $pages;
		}
		
		$this->response($response);	
	}

	function get_year_download_post()
	{
	
		$data = $this->comman_lib->get_offset_limit($this->get_data());
		$result = $this->pictures->get_year_download($data);
		$pages = $this->comman_lib->get_no_of_pages($result['pages']);
		$result = $result['result'];
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				if(!empty($value['categories']))
				{
					$result[$key]['categories'] = $this->category->get_by_ids(explode(',', $value['categories']));
				}
				if(!empty($value['tags']))
				{
					$result[$key]['tags'] = $this->tags->get_by_ids(explode(',', $value['tags']));
				}
			}
		}
		if(empty($result)){
			$response['status'] ="success";
			$response['message'] ="No record found";
			$response['pages'] = 1;
		}else{
			$response['status'] ="success";
			$response['recents'] = $result;
			$response['pages'] = $pages;
		}
		
		$this->response($response);	
	}

	function download_post()
	{
		$data = $this->get_data();	
		$result = $this->pictures->get_picture($data);
		if(!empty($result))
		{
			$download = $this->today_download->check_by_date(date('Y-m-d',time()),$data['ID']);
			//print_r($download);exit;
			if($download)
			{
				$download['downloads'] = $download['downloads'] + 1;
				$this->today_download->update($download);
			}else{
				$download['picture_id'] = $data['ID'];
				$download['date'] = date('Y-m-d',time());
				$download['downloads'] = 1;
				$this->today_download->insert($download);
			}
			$update['total_downloads'] = $result[0]['total_downloads'] + 1;
			$update['id'] = $data['ID'];
			if(!$this->pictures->update($update))
			{
				$response['status'] ="error";
				$response['message'] ="download count update fail";
			}else{
				$response['status'] ="success";
				$response['picture'] = "download count update success";
			}
		}else{
			$response['status'] ="success";
			$response['message'] ="No record found";
		}
		$this->response($response);	
	}

	function get_picture_post()
	{
	
		$data = $this->get_data();
		$result = $this->pictures->get_picture($data);
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				if(!empty($value['categories']))
				{
					$result[$key]['categories'] = $this->category->get_by_ids(explode(',', $value['categories']));
				}
				if(!empty($value['tags']))
				{
					$result[$key]['tags'] = $this->tags->get_by_ids(explode(',', $value['tags']));
				}
				$download = $this->pictures->get_download_by_picture_id($value['id']);
				$result[$key]['weekly_downloads'] = $download['weekly_downloads'];
				$result[$key]['monthly_downloads'] = $download['monthly_downloads'];
				$result[$key]['yearly_downlods'] = $download['yearly_downlods'];
			}
		}
		if(empty($result)){
			$response['status'] ="success";
			$response['message'] ="No record found";
		}else{
			$response['status'] ="success";
			$response['picture'] = $result;
		}
		
		$this->response($response);	
	}
}