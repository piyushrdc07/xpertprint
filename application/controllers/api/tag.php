<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
class Tag extends REST_Controller
{
	
	function __construct() {
		parent::__construct();
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		//load all models
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("category_model", "category", true);
		$this -> load -> model("pictures_model", "pictures", true);
		$this -> load -> model("tags_model", "tags", true);
		
		$headers = $this->input->request_headers();
		$this->comman_lib->valid_hash($headers["Hashkey"]); //HashKey Checking
	}

	function get_tag_post()
	{
	
		$data = $this->comman_lib->get_offset_limit($this->get_data());
		$result = $this->pictures->get_by_tag($data);
		$pages = $this->comman_lib->get_no_of_pages($result['pages']);
		$result = $result['result'];
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				
				if(!empty($value['categories']))
				{
					$result[$key]['categories'] = $this->category->get_by_ids(explode(',', $value['categories']));
				}
				if(!empty($value['tags']))
				{
					$result[$key]['tags'] = $this->tags->get_by_ids(explode(',', $value['tags']));
				}
				$download = $this->pictures->get_download_by_picture_id($value['id']);
				$result[$key]['weekly_downloads'] = $download['weekly_downloads'];
				$result[$key]['monthly_downloads'] = $download['monthly_downloads'];
				$result[$key]['yearly_downlods'] = $download['yearly_downlods'];
			}
		}
		//print_r($result);exit;
		if(empty($result)){
			$response['status'] ="success";
			$response['message'] ="No record found";
			$response['pages'] = 1;
		}else{
			$response['status'] ="success";
			$response['tag'] = $result;
			$response['pages'] = $pages;
		}
		
		$this->response($response);	
	}

	
}