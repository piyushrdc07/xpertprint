<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class tags extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> load -> helper('url');
		$this -> load -> helper('html');
		$this -> load -> database();
		$this -> load -> library('Native_Session');
		$this -> load -> library('Session');
		$this -> load -> library('Message_stack');
		$this -> load -> library('email');
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		
		//load all models
		$this -> load -> model("comman_model", "comman", true);
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("tags_model", "tags", true);
	}

	function index()
	{
		$this->admin->isLogin();
		$data["tags"] = $this->tags->get_all();
		$this->load->view("admin/view_tags",$data);
	}
	
	function add()
	{
		$this->admin->isLogin();
		$request_data = $this->comman_lib->get_data();
		if($request_data["id"] != "")
		{
			$data["record"] = $this->tags->get_by_id($request_data["id"]);
		}
		$this->load->view("admin/add_tags",$data);
	}
	
	function add_post()
	{
		$this->admin->isLogin();
		$class = "danger";
		$request_data = $this->comman_lib->get_data();
		$request_data['tag_date'] = date('Y-m-d');
		
		if(empty($request_data)){
			$url = base_url() . 'tags';
			redirect($url);
		}
		
		
		if($request_data["id"] != ""){
			if($this->tags->update($request_data)){
				$message = "Tags update successfully.";
				$class = "success";
			}else{
				$message = "Internal error occure.";
			}
		}else{
			$id = $this->tags->insert($request_data);
			if(!empty($id)){
				$message = "Tags added successfully.";
				$class = "success";
			}else{
				$message = "Internal error occure.";
			}
		}
		$this -> message_stack -> add_message('message', $message);
		$this -> message_stack -> add_message('class', $class);
		$url = base_url() . 'tags';
		redirect($url);
	}

	function delete()
	{ 
		$class = "danger";
		$this->admin->isLogin();
		$request_data = $this->comman_lib->get_data();
		if(empty($request_data)){
			$url = base_url() . 'tags';
			redirect($url);
		}
		if($this->tags->delete($request_data)){
			$message = "Tags delete successfully.";
			$class = "success";
		}else{
			$message = "Internal error occure.";
		}
		$this -> message_stack -> add_message('message', $message);
		$this -> message_stack -> add_message('class', $class);
		$url = base_url() . 'tags';
		redirect($url);
	}
}