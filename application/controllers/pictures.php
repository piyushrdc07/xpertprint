<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');



class pictures extends CI_Controller {

	function __construct() {

		parent::__construct();

		$this -> load -> helper('url');

		$this -> load -> helper('html');

		$this -> load -> database();

		$this -> load -> library('Native_Session');

		$this -> load -> library('Session');

		$this -> load -> library('Message_stack');

		$this -> load -> library('email');

		$this -> load -> library('comman_lib');

		$this -> load -> library("encrypt");

		

		//load all models

		$this -> load -> model("comman_model", "comman", true);

		$this -> load -> model("admin_model", "admin", true);

		$this -> load -> model("pictures_model", "pictures", true);

		$this -> load -> model("category_model", "category", true);

	}



	function index()

	{

		$this->admin->isLogin();

		$data["categories"] = $this->pictures->get_category();

		$this->load->view("admin/view_pictures",$data);

	}

	

	function view()

	{

		$this->admin->isLogin();

		$request_data = $this->comman_lib->get_data();

		$dataTableColumns = array('image_name','thumb_path','description','location','photographer_name');

        $param = $this->comman_lib->sendCustomParametersWithPagination($request_data,$dataTableColumns);

        $records = $this->pictures->get_all_ajax($param['SortBy'], $param['SortOrder'],$param['Search'],$request_data['iDisplayLength'], $request_data['iDisplayStart'],$request_data);

        $totalRecords = count($this->pictures->get_all_ajax($param['SortBy'], $param['SortOrder'],$param['Search'],0,0,$request_data));

        $data = array();

        foreach ($records as $record) {

        	$path = $this->assets->url('photo.jpg','admin');

        	if($record['thumb_path'] != '')

			{

				$path = base_url().$record['thumb_path'];

			}

            $temp = array(

                $record['image_name'],

                '<img src="'.$path.'" width=50px />',

                $record['description'],

                $record['location'],

                $record['photographer_name'],

                '<a href="'.base_url()."pictures/add?pictureId={$record['id']}".'"><i class="fa fa-pencil"></i></a>

                <a onClick="return confirm(\'Are You Sure Delete Record!\');" href="'.base_url()."pictures/add?pictureId?id={$record['id']}".'"><i class="fa fa-times text-danger"></i></a>'

            );

            array_push($data, $temp);

        }

		//print_r($data);exit;

        $results = array(

            "draw" => $request_data['draw'],

            "recordsTotal" => $totalRecords,

            "recordsFiltered" => $totalRecords,

            "data" => $data

        );

        echo json_encode($results);

	}

	

	function add()

	{

		$this->admin->isLogin();

		$request_data = $this->comman_lib->get_data();

		if($request_data["pictureId"] != "")

		{

			$data["record"] = $this->pictures->get_by_id($request_data["pictureId"]);

		}

		$data["copyrights"] = $this->pictures->get_copyright();

		$data["categories"] = $this->pictures->get_category();

		//echo '<pre>';print_r($data);exit;

		$this->load->view("admin/add_pictures",$data);

	}

	

	function add_post()

	{	

		$this->admin->isLogin();
		$class = "danger";
		$request_data = $this->comman_lib->get_data();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('image_name', 'Name', 'required');
		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('tags', 'Tag', 'required');
		$this->form_validation->set_rules('photo_source_link', 'Photo sorce link', 'required|edit_unique[tbl_picture.photo_source_link.'.$request_data["id"].']');
		if ($this->form_validation->run() == FALSE)
		{
			$message = validation_errors();
			$this -> message_stack -> add_message('message', $message);
			$this -> message_stack -> add_message('class', $class);
			if($request_data["id"] != ""){
				$url = base_url() . 'pictures/add?pictureId='.$request_data["id"];
			}else{
				$url = base_url() . 'pictures/add';
			}
			redirect($url);
		}else{

			if (!empty($request_data['category_id'])) {

				$request_data['category_id'] = implode($request_data['category_id'], ',');

			}

			

			$tags = explode(',',$request_data['tags']);

			

			$tag_ids = array();

			foreach($tags as $tag){

				$tagId = $this->pictures->check_tags($tag);

				

				if($tagId > 0){

					array_push($tag_ids,$tagId);

				}else{

					$tagId = $this->pictures->add_tags($tag);

					array_push($tag_ids,$tagId);

				}	

			}

			

			$request_data['tags'] = implode(',',$tag_ids);

			$request_data['tags'] = rtrim($request_data['tags'],",");

			//echo "<pre>";print_r($request_data);exit;

			$request_data['upload_date'] = date('Y-m-d');

			

			if(empty($request_data)){

				$url = base_url() . 'pictures';

				redirect($url);

			}

			

			//Image Upload

			if ($_FILES['Image']['name'] != '') {

				$oldFileName = $request_data['image_name'];

				$path = APPPATH.'upload/';

				$allowedTypes = 'gif|jpg|png|jpeg';

				$fieldname = 'Image';

				$folderName = "picture";

				$result = $this->comman_lib->uploadPhoto($fieldname, $path, $allowedTypes, $_FILES,$folderName,true,512,true);

				if (!empty($result)) {

					//$request_data['image_name'] = $result;

					$request_data['image_path'] = 'application/upload/'.$folderName."/".$result;

					$request_data['thumb_path'] = 'application/upload/'.$folderName."/thumb_small/".$result;

					

					$request_data['360_path'] = 'application/upload/'.$folderName."/thumb_360/".$result;

					$request_data['720_path'] = 'application/upload/'.$folderName."/thumb_720/".$result;

					

					list($request_data['image_width'],$request_data['image_height']) = getimagesize(base_url().$request_data['image_path']);

					list($request_data['thumb_width'],$request_data['thumb_height']) = getimagesize(base_url().$request_data['thumb_path']);

					

					list($request_data['360_width'],$request_data['360_height']) = getimagesize(base_url().$request_data['360_path']);

					list($request_data['720_width'],$request_data['720_height']) = getimagesize(base_url().$request_data['720_path']);

					unlink($path.$folderName."/".$oldFileName);

					unlink($path.$folderName."/thumb_small/".$oldFileName);

					unlink($path.$folderName."/thumb_720/".$oldFileName);

					unlink($path.$folderName."/thumb_360/".$oldFileName);

				}

			}



			if ($_FILES['ImagePhoto']['name'] != '') {

				$oldFileName = $request_data['photographer_photo'];

				$path = APPPATH.'upload/';

				$allowedTypes = 'gif|jpg|png|jpeg';

				$fieldname = 'ImagePhoto';

				$folderName = "picture";

				$result = $this->comman_lib->uploadPhoto($fieldname, $path, $allowedTypes, $_FILES,$folderName,true,512);

				if (!empty($result)) {

					$request_data['photographer_photo'] = 'application/upload/'.$folderName."/thumb_small/".$result;

					unlink($path.$folderName.'/'.$request_data['photographer_photo']);

					unlink($path.$folderName.'/'.$oldFileName);

				}

			}

			//echo "<pre>";print_r($request_data);exit;

			//End image upload

			

			

			if($request_data["id"] != ""){

				if($this->pictures->update($request_data)){

					$message = "Picture update successfully.";

					$class = "success";

				}else{

					$message = "Internal error occure.";

				}

			}else{

				$id = $this->pictures->insert($request_data);

				if(!empty($id)){

					$message = "Picture added successfully.";

					$class = "success";

				}else{

					$message = "Internal error occure.";

				}

			}

			$this -> message_stack -> add_message('message', $message);

			$this -> message_stack -> add_message('class', $class);

			$url = base_url() . 'pictures';

			redirect($url);
		}

	}



	function delete()

	{ 

		$class = "danger";

		$this->admin->isLogin();

		$request_data = $this->comman_lib->get_data();

		if(empty($request_data)){

			$url = base_url() . 'pictures';

			redirect($url);

		}

		if($this->pictures->delete($request_data)){

			$message = "pictures delete successfully.";

			$class = "success";

		}else{

			$message = "Internal error occure.";

		}

		$this -> message_stack -> add_message('message', $message);

		$this -> message_stack -> add_message('class', $class);

		$url = base_url() . 'pictures';

		redirect($url);

	}

	

	

}