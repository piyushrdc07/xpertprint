<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');



class Category extends CI_Controller {

	function __construct() {

		parent::__construct();

		$this -> load -> helper('url');

		$this -> load -> helper('html');

		$this -> load -> database();

		$this -> load -> library('Native_Session');

		$this -> load -> library('Session');

		$this -> load -> library('Message_stack');

		$this -> load -> library('email');

		$this -> load -> library('comman_lib');

		$this -> load -> library("encrypt");

		

		//load all models

		$this -> load -> model("comman_model", "comman", true);

		$this -> load -> model("admin_model", "admin", true);

		$this -> load -> model("category_model", "category", true);

	}



	function index()

	{

		$this->admin->isLogin();

		$this->load->view("admin/view_category");

	}
	
	function view()
	{
		$this->admin->isLogin();
		$request_data = $this->comman_lib->get_data();
		$dataTableColumns = array('category','status');
        $param = $this->comman_lib->sendCustomParametersWithPagination($request_data,$dataTableColumns);
        $records = $this->category->get_all_ajax($param['SortBy'], $param['SortOrder'],$param['Search'],$request_data['iDisplayLength'], $request_data['iDisplayStart'],$request_data);
        $totalRecords = count($this->category->get_all_ajax($param['SortBy'], $param['SortOrder'],$param['Search'],0,0,$request_data));
        $data = array();
        foreach ($records as $record) {
        	$status = 'Inactive';
        	if($record['status'] == 'y')
			{
				$status = 'Active';
			}
            $temp = array(
                $record['category'],
                $status,
                '<a href="'.base_url()."category/add?cateId={$record['id']}".'"><i class="fa fa-pencil"></i></a>
                <a onClick="return confirm(\'Are You Sure Delete Record!\');" href="'.base_url()."category/delete?id={$record['id']}".'"><i class="fa fa-times text-danger"></i></a>'
            );
            array_push($data, $temp);
        }
        $results = array(
            "draw" => $request_data['draw'],
            "recordsTotal" => $totalRecords,
            "recordsFiltered" => $totalRecords,
            "data" => $data
        );
        echo json_encode($results);
	}

	

	function add()

	{

		$this->admin->isLogin();

		$request_data = $this->comman_lib->get_data();

		if($request_data["cateId"] != "")

		{

			$data["record"] = $this->category->get_by_id($request_data["cateId"]);

		}

		$this->load->view("admin/add_category",$data);

	}

	

	function add_post()

	{

		$this->admin->isLogin();

		$class = "danger";

		$request_data = $this->comman_lib->get_data();

		

		if(empty($request_data)){

			$url = base_url() . 'category';

			redirect($url);

		}

		//print_r($request_data);exit;

		

		//Image Upload

		if ($_FILES['Image']['name'] != '') {

			$oldFileName = $request_data['photo'];

			$path = APPPATH.'upload/';

			$allowedTypes = 'gif|jpg|png|jpeg';

			$fieldname = 'Image';

			$folderName = "category";

			$result = $this->comman_lib->uploadPhoto($fieldname, $path, $allowedTypes, $_FILES,$folderName,true,512);

			if (!empty($result)) {

				$request_data['photo'] = $result;

				unlink($path.$folderName."/".$oldFileName);

				unlink($path.$folderName."/thumb_small/".$oldFileName);

			}

		}

		//End image upload

		

		

		if($request_data["id"] != ""){

			if($this->category->update($request_data)){

				$message = "Category update successfully.";

				$class = "success";

			}else{

				$message = "Internal error occure.";

			}

		}else{

			$id = $this->category->insert($request_data);

			if(!empty($id)){

				$message = "Category added successfully.";

				$class = "success";

			}else{

				$message = "Internal error occure.";

			}

		}

		$this -> message_stack -> add_message('message', $message);

		$this -> message_stack -> add_message('class', $class);

		$url = base_url() . 'category';

		redirect($url);

	}



	function delete()

	{ 

		$class = "danger";

		$this->admin->isLogin();

		$request_data = $this->comman_lib->get_data();

		if(empty($request_data)){

			$url = base_url() . 'category';

			redirect($url);

		}

		if($this->category->delete($request_data)){

			$message = "Category delete successfully.";

			$class = "success";

		}else{

			$message = "Internal error occure.";

		}

		$this -> message_stack -> add_message('message', $message);

		$this -> message_stack -> add_message('class', $class);

		$url = base_url() . 'category';

		redirect($url);

	}

}