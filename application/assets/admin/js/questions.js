$(document).ready(function(){
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#MinDate').datepicker({
    	onRender: function(date) {
    		return date.valueOf() < now.valueOf() ? 'disabled' : '';
    	}
    }).on('changeDate', function(ev) {
    	if (ev.date.valueOf() > checkout.date.valueOf()) {
    		var newDate = new Date(ev.date)
		    newDate.setDate(newDate.getDate() + 1);
		    checkout.setValue(newDate);
    	}
    	checkin.hide();
    	$('#MaxDate')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#MaxDate').datepicker({
    	onRender: function(date) {
    		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    	}
    }).on('changeDate', function(ev) {
    	checkout.hide();
    }).data('datepicker');
	    
	    
	$(".dyna").hide();
	var selectedOption = $("#hiddenFieldType").val();
	$("#FieldType").val(selectedOption).change();
});

function generateFields(obj) {
	var option = obj.options[obj.selectedIndex].value;
	var count = obj.options.length;
	for (var i = 0; i < count; i++) {
		if (obj.options[i].value == option) {
			var subtype = obj.options[i].value;
			// setQuesTitle(subtype);
			switch(subtype) {
			    case "ALPHANUMERIC": 	dynamicGenerator(".MaxCharacter");
								        break;
			    case "POSTALCODE": 		dynamicGenerator("");
			    						break;
			    case "CURRENCY": 		dynamicGenerator(".CurrencySymbol");
			    						break;
			    case "DATE": 			dynamicGenerator(".Date");
			    						break;
			    case "FLOAT": 			dynamicGenerator(".MaxCharacter");
			    						break;
			    case "EMAIL": 			dynamicGenerator(".AutoReply");
			    						break;
			    case "GENDER": 			dynamicGenerator("");
			    						break;
			    case "DISPLAY_IMAGE": 	dynamicGenerator("");
			    						break;
			    case "LIST_PICKER": 	dynamicGenerator("");
			    						break;
			    case "MAP_POINT": 		dynamicGenerator("");
			    						break;
			    case "LIKERT_SCALE": 	dynamicGenerator("");
			    						break;
			   case "MULTISELECT_IMAGE":dynamicGenerator("");
			    						break;
			   case "MULTICHOICE_IMAGE":dynamicGenerator("");
			    						break;
			    case "MULTISELECT": 	dynamicGenerator(".Response");
			    						break;
			    case "MULTICHOICE": 	dynamicGenerator(".IncludeOther");
			    						break;
			    case "INTEGER": 		dynamicGenerator(".MaxCharacter");
			    						break;		
				case "PDF": 			dynamicGenerator("");
			    						break;		
			    case "PHOTO": 			dynamicGenerator("");
			    						break;
			    case "QRCODE": 			dynamicGenerator("");
			    						break;
			case "SATISFACTION_RATING": dynamicGenerator("");
			    						break;		        				
			   case "SWIPE_TO_REVEAL": 	dynamicGenerator("");
			    						break;
			    case "SIGNATURE": 		dynamicGenerator("");
			    						break;
			    case "STAR_RATING": 	dynamicGenerator("");
			    						break;						
			    case "STOP_WATCH": 		dynamicGenerator("");
			    						break;			
			    case "SUMMARY": 		dynamicGenerator(".ButtonText");
			    						break;							
			    case "TELEPHONE": 		dynamicGenerator(".ShowExtension");
			    						break;	
		case "INTERNATIONAL_TELEPHONE": dynamicGenerator(".MaxCharacter");
								        break;	
				case "NOTICE": 			dynamicGenerator("");
			    						break;					
			    case "DISPLAY_TEXT": 	dynamicGenerator(".DisplayText, .ButtonText");
			    						break;
			    case "TEXTAREA": 		dynamicGenerator(".MaxCharacter");
								        break;						
			    case "TRUE_FALSE": 		dynamicGenerator("");
								        break;						
			    case "ZIPCODE": 		dynamicGenerator("");
								        break;							
			 case "WEBVIEW_FULLSCREEN": dynamicGenerator("");
								        break;
				case "YES_NO": 			dynamicGenerator("");
								        break;
				case "DISPLAY_VIDEO": 	dynamicGenerator("");
								        break;	        				        																					
			    default:				dynamicGenerator("");
								        break;
			} 
		}
	}
}

function dynamicGenerator(visible_field) {
	$(".dyna").hide();
	$(visible_field).show();
}

function setQuesTitle(question_type) {
	var question = $("#question-title");
	switch(question_type) {
			    case "EMAIL": 			question.val("What is your email address?");
			    						break;
			    case "GENDER": 			question.val("What is your gender?");
			    						break;
			    case "INTEGER": 		dynamicGenerator(".MaxCharacter");
			    						break;		
			    case "TELEPHONE": 		dynamicGenerator(".ShowExtension");
			    						break;	
		case "INTERNATIONAL_TELEPHONE": dynamicGenerator(".MaxCharacter");
								        break;	
				case "NOTICE": 			dynamicGenerator("");
			    						break;					
			    case "DISPLAY_TEXT": 	dynamicGenerator(".DisplayText, .ButtonText");
			    						break;
			    case "TEXTAREA": 		dynamicGenerator(".MaxCharacter");
								        break;						
			    case "TRUE_FALSE": 		dynamicGenerator("");
								        break;						
			    case "ZIPCODE": 		dynamicGenerator("");
								        break;							
			 case "WEBVIEW_FULLSCREEN": dynamicGenerator("");
								        break;
				case "YES_NO": 			dynamicGenerator("");
								        break;
				case "DISPLAY_VIDEO": 	dynamicGenerator("");
								        break;	        				        																					
			    default:				ques.val("");
								        break;
			} 
}

function refreshDisplayFields(obj) {
	var option = obj.options[obj.selectedIndex].value;
	var count = obj.options.length;
	for (var i = 0; i < count; i++) {
		if (obj.options[i].value == option) {
			var subtype = obj.options[i].value;
			if (subtype == "LIST_PICKER" || subtype == "MULTISELECT" || subtype == "MULTICHOICE" || subtype == "RANKING" || subtype == "SLIDER") {
				$('.PossibleAnswer').show();
				$('.Shortcuts').hide();
				setShortcutTextValues(subtype);
			} else if(subtype == "EMAIL" || subtype == "TEXTFIELD" || subtype == "WEBSITE") {
				$('.Shortcuts').show();
				setShortcutTextValues(subtype);
				$('.PossibleAnswer').hide();
			} else {
				$('.PossibleAnswer').hide();
				$('.Shortcuts').hide();
				setShortcutTextValues(subtype);
			}
		}
	}
}

function setShortcutTextValues(subtype) {
	if (subtype == "EMAIL") {
		$(".Shortcuts input[name='Shortcut1']").val("@gmail.com");
		$(".Shortcuts input[name='Shortcut2']").val("@hotmail.com");
		$(".Shortcuts input[name='Shortcut3']").val("@yahoo.com");
		$(".Shortcuts input[name='Shortcut4']").val("@aol.com");
	} else if(subtype == "WEBSITE") {
		$(".Shortcuts input[name='Shortcut1']").val("http://");
		$(".Shortcuts input[name='Shortcut2']").val("www.");
		$(".Shortcuts input[name='Shortcut3']").val(".org");
		$(".Shortcuts input[name='Shortcut4']").val(".net");
	} else {
		$(".Shortcuts input").val("");
	}
}
