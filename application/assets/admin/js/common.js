var site_url = window.location.protocol + "//" + window.location.host+"/BackgroundHD/";
if(window.location.host == "localhost")
{
	site_url = window.location.protocol + "//" + window.location.host + "/BackgroundHD/";
}
function AjaxCall(URL, Type, PostData, DataType, Method, ParamObj) {
    $.ajax({
        url: URL,
        type: Type,
        data: PostData,
        cache: false,
        dataType: DataType,
        success: function (data) {
            if (ParamObj == undefined || ParamObj == null || ParamObj == "") {
                window[Method](data);
            }
            else {
                window[Method](data, ParamObj);
            }
        },
        error: function (e, errorMsg) {
            window["errorAlert"](e, errorMsg, ParamObj);
        }

    });
}

function AjaxFileCall(URL, Type, PostData, Method) {
    $.ajax({
        url: URL,
        type: Type,
        data: PostData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            window[Method](data);
        },
        error: errorAlert
    });
}

function errorAlert(e, errorMsg, ParamObj) {
    alert("Your request was not successful: " + errorMsg);    
    //LoadMask(false);
};

function show_loader(){
	error_message_close();
	$('.overlay').removeClass('hide');
}
function hide_loader(){
	$('.overlay').addClass('hide');
}

function error_message_html(){
	return '<div class="alert alert-_CLASS_"><button data-dismiss="alert" class="close" type="button">×</button><i class="fa fa-ok-sign"></i>_MESSAGE_</div>';
}

function error_message_close(){
	$(".ajax-message").html('').addClass('hide');
}
