<?php $this->load->view("admin/header"); ?>

    <script type="text/javascript">
        var PIXABAY_API_KEY = '2614071-36b6d7534db4c53c1cbc98002';
        $(document).ready(function(){

            $.each($(".left-side-menu-bar li"),function(key,value){

                if($(value).attr('pagename') == 'pixabay')

                {

                    $(value).attr("class","active");

                }

            });

            $('.filestyle').change(function(event){

                var tmppath = URL.createObjectURL(event.target.files[0]);

                $(".preview").fadeIn("fast").attr('src',tmppath);

            });

            $("#get_pixabay_image").click(function(){
                fetch_pixabay_image($("#image_id").val())
            });


        });

        function fetch_pixabay_image($id)
        {
            var URL = "https://pixabay.com/api/?key="+PIXABAY_API_KEY+"&id="+encodeURIComponent($id);
            $.getJSON(URL, function(data){
                if (parseInt(data.totalHits) > 0)
                    console.log(data);
                else
                    console.log('No hits');
            });
        }

    </script>

    <section class="vbox" id="bjax-el">

        <section class="scrollable wrapper-lg">

            <div class="row">

                <section class="panel panel-default col-md-12">

                    <header class="panel-heading font-bold">Add Pixabay Image</header>

                    <div class="panel-body">

                        <form name="categoty_form" id="categoty_form" class="form-horizontal" data-validate="parsley" action="#<?php echo base_url(); ?>tags/add_post" method="post" enctype="multipart/form-data">

                            <div class="form-group">

                                <label class="col-sm-2 control-label" for="input-id-1">Pixabay Image ID</label>

                                <div class="col-sm-4">

                                    <input type="text" name="image_id" class="form-control" id="image_id" value="11574" data-required="true">

                                </div>
                                <div class="col-sm-2">
                                    <button id="get_pixabay_image" type="button" class="btn btn-info">Get Image</button>
                                </div>

                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">

                                <div class="col-sm-4 col-sm-offset-2">

                                    <button type="submit" class="btn btn-info">Save</button>

                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url(); ?>tags'">Cancel</button>

                                </div>

                            </div>

                        </form>

                    </div>

                </section>

            </div>

        </section>

    </section>

<?php $this->load->view("admin/footer"); ?>