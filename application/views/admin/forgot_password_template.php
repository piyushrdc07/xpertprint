<html>
<head>
	<title>Kiosklead Password Reset</title>
</head>

<body>
<div style="width:500px; height: 233px; margin: 0 auto; background-color: #ffffff; border: 1px solid #ccc; padding: 20px;">
	<div style=" float: right; width: 200px;">
	<div style=" margin-bottom: 10px;">
		<a href="<?php echo base_url(); ?>">
			<img height="20px" width="150px" title="Kiosklead" alt="Kiosklead" src="<?php echo $this->assets->url('logo.png'); ?>">
		</a>
	</div>
	
    </div>
    <div style="float:left; width: 260px;">
	<h3 style=" font-family:'Helvetica'; font-size:18px">Hello,</h3>
	<p style=" font-family:'Helvetica'; font-size: 13px; line-height: 22px;margin-bottom:20px;">
		Hi, <?php echo $data['Name']; ?></br>
		We have received a request to reset the password associated with this email address.
	</p>
	<p style=" font-family:'Helvetica'; font-size: 13px; line-height: 22px;margin-bottom:20px;">
		To reset your password now, click the link below.
	</p>
	<a style=" background: none repeat scroll 0 0 #018AD8; border: 1px solid #00507E; border-radius: 4px 4px 4px 4px; box-shadow: 0 0 1px 1px rgba(255, 255, 255, 0.3) inset;
    color: #FFFFFF; cursor: pointer;font-size: 18px; padding: 7px 15px 6px; text-align: center;text-decoration: none;font-family:'Helvetica';vertical-align:bottom;" href="<?php echo $data['URL']; ?>" target="_blank"">Reset</a>
	</div>
</div>

</body>
</html>