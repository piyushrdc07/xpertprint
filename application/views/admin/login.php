<!DOCTYPE html>
<html lang="en" class="app">
<head>  
  <meta charset="utf-8" />
  <title>Sign in by Background HD </title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <?php
    //$this->assets->load("js/jPlayer/jplayer.flat.css",'admin');
	$this->assets->load("bootstrap.css",'admin');
	$this->assets->load("animate.css",'admin');
	//for local
	$this->assets->load("font-awesome.min.css",'admin');
	$this->assets->load("simple-line-icons.css",'admin');
	//End for local
	$this->assets->load("font.css",'admin');
	$this->assets->load("app.css",'admin');
	//load javascript file
	
	$this->assets->load("jquery.min.js",'admin');
	$this->assets->load("bootstrap.js",'admin');
	$this->assets->load("app.js",'admin');
	$this->assets->load("slimscroll/jquery.slimscroll.min.js",'admin');
	$this->assets->load("app.plugin.js",'admin');
    echo $this->assets->display_header_assets();
	
?>

    <!--[if lt IE 9]>
    <script src="<?php echo $this->assets->url('ie/html5shiv.js','admin'); ?>"></script>
    <script src="<?php echo $this->assets->url('ie/respond.min.js','admin'); ?>"></script>
    <script src="<?php echo $this->assets->url('ie/excanvas.js','admin'); ?>js/"></script>
  <![endif]-->
</head>
<body class="bg-info dker">
  <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xl">
      <a class="navbar-brand block" href="<?php echo base_url(); ?>"><span class="h1 font-bold">Background HD</span></a>
      <section class="m-b-lg">
        <header class="wrapper text-center">
          <strong>Sign in to get in touch</strong>
        </header>
        <!-- success or Error Message Display -->
        <?php
        	$message = $this->message_stack->message('message');
			if($message != ""){
        ?>
        <div class="alert alert-<?php echo $this->message_stack->message('class') ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <i class="fa fa-ok-sign"></i><?php echo $message; ?>
        </div>
        <?php } ?>
        <!-- End success or Error Message Display -->
        <form id="loginForm" name="loginForm" method="post" action="<?php echo base_url().'admin/login'; ?>" >
          <div class="form-group">
            <input name="Email" type="email" placeholder="Email" class="form-control rounded input-lg text-center no-border">
          </div>
          <div class="form-group">
             <input name="Password" type="password" placeholder="Password" class="form-control rounded input-lg text-center no-border">
          </div>
          
          <button type="submit" class="btn btn-lg btn-warning lt b-white b-2x btn-block btn-rounded"><i class="icon-arrow-right pull-right"></i><span class="m-r-n-lg">Sign in</span></button>
          
          <!-- <div class="text-center m-t m-b"><a href="<?php echo base_url(); ?>admin/forgotPassword"><small>Forgot password?</small></a></div> -->
          <div class="line line-dashed"></div>
          <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
          <a href="<?php echo base_url(); ?>admin/register" class="btn btn-lg btn-info btn-block rounded">Sign Up</a> -->
        </form>
      </section>
    </div>
  </section>
  <script type="text/javascript">
  	$('#loginForm').submit(function(){
	    $('button', this).attr('disabled', 'disabled');
	});
  </script>
  <!-- footer -->
  <footer id="footer">
    <div class="text-center padder">
      <p>
        <small>&copy; <?php echo date('Y'); ?> Background HD. All rights reserved.</small>
      </p>
    </div>
  </footer>
</body>
</html>