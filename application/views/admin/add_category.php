<?php $this->load->view("admin/header"); ?>

<script type="text/javascript">

	$(document).ready(function(){

		$.each($(".left-side-menu-bar li"),function(key,value){

			if($(value).attr('pagename') == 'category')

			{

				$(value).attr("class","active");

			}

		});

		$('.filestyle').change(function(event){

			var tmppath = URL.createObjectURL(event.target.files[0]);

			$(".preview").fadeIn("fast").attr('src',tmppath);

		});

		

		$("#dpd1").datepicker({

			changeMonth: true,

			changeYear: true,

			yearRange: "-100:+0",

			dateFormat : 'dd-mm-yy',

		});

		

		/*$("#dpd2").timepicker({

			//showOn: "button",

			//buttonImage:image_path,

			//buttonImageOnly: true,

			showSecond:true,

			timeFormat: 'hh:mm:ss',

			//changeMonth: true,

			//changeYear: true,

			//yearRange: "-100:+0",

			//dateFormat : 'dd-mm-yy',

		});*/

		//CKEDITOR.replace('page_content');

	});

</script>

<section class="vbox" id="bjax-el">

	<section class="scrollable wrapper-lg">

		<div class="row">

			<section class="panel panel-default col-md-12">

				<header class="panel-heading font-bold"><?php if(!empty($record) && $record['id'] != ""){ echo "Edit"; }else{ echo "Add"; } ?> Category</header>

				<div class="panel-body">

					<form name="categoty_form" id="categoty_form" class="form-horizontal" data-validate="parsley" action="<?php echo base_url(); ?>category/add_post" method="post" enctype="multipart/form-data">

						<input type="hidden" name="id" value="<?php if(!empty($record) && $record['id'] != ""){ echo $record['id']; } ?>" />

						

						<div class="form-group">

							<label class="col-sm-2 control-label" for="input-id-1">Category name</label>

							<div class="col-sm-4">

								<input type="text" name="category" class="form-control" id="input-id-1" value="<?php if(!empty($record) && $record['category'] != ""){ echo $record['category']; } ?>" data-required="true">

							</div>

						</div>

						<?php $required = $record['id']==0?'data-required="true"':''; ?>

						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Category image</label>
							<div class="col-sm-4">
								<input type="hidden" name="photo" value="<?php if(!empty($record) && $record['photo'] != ""){ echo $record['photo']; } ?>">
								<input name="Image" id="Image" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-4">
							<?php
								$deafultImage = $this->assets->url('photo.jpg','admin');
								if(!empty($record) && $record['photo'] != "")
								{
									$deafultImage = base_url()."application/upload/category/thumb_small/".$record['photo'];
								}
							?>
								<img width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
							</div>
						</div>

						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">

							<label class="col-sm-2 control-label" for="input-id-1">Status</label>

							<div class="col-sm-4">

							<?php 

							$checkedy = $record['status']=='y'?"checked":"";

							$checkedn = $record['status']=='n'?"checked":""; ?>

							<input type="radio" name="status" value="y" data-required="true" <?php echo $checkedy; ?>>Yes<br>

							<input type="radio" name="status" value="n" data-required="true" <?php echo $checkedn; ?>>No<br>

							</div>

						</div>

						

		                

						<div class="line line-dashed b-b line-lg pull-in"></div>

						<div class="form-group">

							<div class="col-sm-4 col-sm-offset-2">

								<button type="submit" class="btn btn-info">Save</button>

								<button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url(); ?>category'">Cancel</button>

							</div>

						</div>

					</form>

				</div>

			</section>

		</div>

	</section>

</section>
<?php
	$this->assets->load("file-input/bootstrap-filestyle.min.js",'admin');
	echo $this->assets->display_header_assets();
?>
<?php $this->load->view("admin/footer"); ?>