<?php $this->load->view('admin/header');?>
<script type="text/javascript">
	$(document).ready(function(){
	    /*$.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'category')
			{
				$(value).attr("class","active");
			}
		});*/
    });
</script>
<section class="vbox">
	<section class="scrollable padder">
		<div class="m-b-md">
			<!-- <h3 class="m-b-none">Manage User</h3> -->
		</div>
		<!-- success or Error Message Display -->
        <?php
        	$message = $this->message_stack->message('message');
			if($message != ""){
        ?>
        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <i class="fa fa-ok-sign"></i><?php echo $message; ?>
        </div>
        <?php } ?>
        <div class="ajax-message hide">
        </div>
        <!-- End success or Error Message Display -->
		<div class="row"></div>
		<section class="panel panel-blue">
		  <header class="panel-heading"> Dashboard Image List<a class="pull-right" href="<?php echo base_url()."admin/dashboard_add" ?>"><i style="color:#FFF;" class="fa fa-plus fa-1x">&nbsp;Add Dashboard Image</i></a> </header>
		  <div class="table-responsive">
		    <table class="table table-striped b-t b-light">
                  <thead>
                    <tr>
                      <th class="th-sortable" data-toggle="class" width="50%" >Image</th>
                      <th class="th-sortable" data-toggle="class" width="40%" >Display Order</th>
                      <th class="edit-delet-action" width="10%" >Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php foreach($dashboard as $image): ?>
                  		<tr>
				          <!-- <td><?php echo $image["Photo"]; ?></td> -->
				          <td>
				          	<?php
								$deafultImage = $this->assets->url('photo.jpg','admin');
								if(!empty($image) && $image['Photo'] != "")
								{
									$deafultImage = base_url()."application/upload/DefaultImage/thumb_small/".$image['Photo'];
								}
							?>
								<img width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
				          </td>
				          <td><?php echo $image["DisplayOrder"]; ?></td>
	                      <td class="edit-delet-action">
	                      	<a href="<?php echo base_url()."admin/dashboard_add?DashboardID={$image['DashboardID']}" ?>"><i class="fa fa-pencil"></i></a>
	                      	<a onClick="return confirm('Are You Sure Delete Record!');" href="<?php echo base_url()."admin/dashboard_delete?DashboardID={$image['DashboardID']}" ?>"><i class="fa fa-times text-danger"></i></a>
	                      </td>
	                    </tr>	
                  	<?php endforeach; ?>
                  </tbody>
                </table>
		  </div>
		  </section>
	</section>
</section>
<?php $this->load->view('admin/footer');?>
