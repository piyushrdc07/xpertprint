<?php $this->load->view("admin/header"); ?>
    <script type="text/javascript">
        var FLICKR_API_KEY = 'c1dd89688219e3bb338ce53a91c89c7d';
        var FLICKR_API_URL = "https://api.flickr.com/services/rest";
        var FLICKR_COPYRIGHT = {
            0 : '3',1:'13',2:'14',3:'12',4:'5',5:'8',6:'4'
        };
        $(document).ready(function(){
            $.each($(".left-side-menu-bar li"),function(key,value){
                if($(value).attr('pagename') == 'flickr')
                {
                    $(value).attr("class","active");
                }
            });
            $('.filestyle').change(function(event){
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $(".preview").fadeIn("fast").attr('src',tmppath);
            });
            $("#get_pixabay_image").click(function(){
                var _img_id = $("#image_id").val();
                if(_img_id == 0)
                {
                    alert("Please enter valid image id");
                    return false;
                }
                var postData = {
                    method:'flickr.photos.getInfo',
                    api_key:FLICKR_API_KEY,
                    photo_id:_img_id,
                    format:'json',
                    nojsoncallback:1
                };
                var type = 'POST';
                var dataType = 'JSON';
                AjaxCall(FLICKR_API_URL,type,postData,dataType,'fetch_pixabay_image');
            });
        });

        function image_response(data,image)
        {
            if (data.stat == 'ok')
            {
                var image_sizes = data.sizes.size;
                $.each(image_sizes,function(key, size){
                    if(size.width >= '2048')
                    {
                        image.hdwidth = size.width;
                        image.hdheight = size.height;
                        image.hdpath = size.source;
                    }
                    if(size.label == 'Thumbnail')
                    {
                        image.thumbnail = size.source;
                    }
                });
                console.log(image);
                image.tag_csv = '';
                var tags = image.tags.tag;
                $.each(tags,function(key, tag){
                    if(image.tag_csv == '')
                    {
                        image.tag_csv = tag.raw;
                    }else{
                        image.tag_csv = image.tag_csv+","+tag.raw;
                    }
                });
                set_data_to_form(image);
            }
            else
            {
                console.log('No hits');
            }
        }

        function fetch_pixabay_image(data)
        {
            if (data.stat == 'ok')
            {
                var postData = {method:'flickr.photos.getSizes',api_key:FLICKR_API_KEY,photo_id:data.photo.id,format:'json',nojsoncallback:1};
                var type = 'POST';
                var dataType = 'JSON';
                AjaxCall(FLICKR_API_URL,type,postData,dataType,'image_response',data.photo);
            }
            else
            {
                console.log('No hits');
            }
        }

        function set_data_to_form(data)
        {
            var owner_img = "https://farm"+data.owner.iconfarm+".staticflickr.com/"+data.owner.iconserver+"/buddyicons/"+data.owner.nsid+"_r.jpg";
            $("#image_name").val(data.title._content);
            $("#image_path").val(data.hdpath);
            $("#ImagePreview").attr('src',data.thumbnail);
            $("#description").text(data.description._content);
            $("#photographer_name").val(data.owner.realname);
            $("#photographer_photo").val(owner_img);
            $("#ImagePhotoPreview").attr('src',owner_img);
            $("#photo_source_link").val(data.urls.url[0]._content);
            console.log(FLICKR_COPYRIGHT[data.license]);
            $("#copyright_id").val(FLICKR_COPYRIGHT[data.license]);
            $("#tags").val(data.tag_csv);
        }
    </script>

    <section class="vbox" id="bjax-el">
        <section class="scrollable wrapper-lg">
            <!-- success or Error Message Display -->
            <?php
            $message = $this->message_stack->message('message');
            if($message != ""){
                ?>
                <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <i class="fa fa-ok-sign"></i><?php echo $message; ?>
                </div>
            <?php } ?>
            <div class="ajax-message hide">
            </div>
            <!-- End success or Error Message Display -->

            <div class="row">
                <section class="panel panel-default col-md-12">
                    <header class="panel-heading font-bold">Add Flickr Image</header>
                    <div class="panel-body">
                        <form name="categoty_form" id="categoty_form" class="form-horizontal" data-validate="parsley" action="<?php echo base_url(); ?>flickr/add_post" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Pixabay Image ID</label>
                                <div class="col-sm-4">
                                    <input type="text" name="image_id" class="form-control" id="image_id" value="27168379265">
                                </div>
                                <div class="col-sm-2">
                                    <button id="get_pixabay_image" type="button" class="btn btn-info">Get Image</button>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <?php $required = $record['id']==0?'data-required="true"':''; ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="image_name">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" name="image_name" class="form-control" id="image_name" value="<?php echo $record['image_name']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <input id="image_path" type="hidden" name="image_path" value="">
                                    <?php
                                    $deafultImage = $this->assets->url('photo.jpg','admin');
                                    if(!empty($record) && $record['thumb_path'] != "")
                                    {
                                        $deafultImage = base_url().$record['thumb_path'];
                                    }
                                    ?>
                                    <img id="ImagePreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Description</label>
                                <div class="col-sm-4">
                                    <textarea id="description" type="textarea" readonly="readonly" name="description" class="form-control" data-required="true"><?php echo $record['description']; ?></textarea>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Photographer</label>
                                <div class="col-sm-4">
                                    <input type="text" readonly="readonly" name="photographer_name" class="form-control" id="photographer_name" value="<?php echo $record['photographer_name']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <input id="photographer_photo" type="hidden" name="photographer_photo" value="">
                                    <?php
                                    $deafultImage = $this->assets->url('photo.jpg','admin');
                                    if(!empty($record) && $record['photographer_photo'] != "")
                                    {
                                        $deafultImage = base_url().$record['photographer_photo'];
                                    }
                                    ?>
                                    <img id="ImagePhotoPreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Photo sorce link</label>
                                <div class="col-sm-4">
                                    <input type="text" readonly="readonly" name="photo_source_link" class="form-control" id="photo_source_link" value="<?php echo $record['photo_source_link']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Category</label>
                                <div class="col-sm-4">
                                    <select style="height:300px;" class="form-control m-b parsley-validated" name="category_id[]" multiple="" data-required="true">
                                        <?php foreach($categories as $category):
                                            $selected = '';
                                            if(!empty($record['category_id']))
                                            {
                                                $categories = explode(',', $record['category_id']);
                                                if(in_array($category['id'],$categories))
                                                {
                                                    $selected = 'selected="selected"';
                                                }
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Copyright</label>
                                <div class="col-sm-4">
                                    <select id="copyright_id" disabled="disabled" class="form-control m-b parsley-validated" name="copyright_id" data-required="true">
                                        <option value="">Please choose</option>
                                        <?php foreach($copyrights as $copyright):
                                            $selected = $copyright['id']==$record['copyright_id']?"selected=selected":"";
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $copyright['id']; ?>"><?php echo $copyright['copyright_title']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Tags</label>
                                <div class="col-sm-4">
                                    <input id="tags" type="textbox" class="form-control" name="tags" value="<?php echo $record['tag_name']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button type="submit" class="btn btn-info">Save</button>
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url(); ?>tags'">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </section>
    </section>
<?php $this->load->view("admin/footer"); ?>