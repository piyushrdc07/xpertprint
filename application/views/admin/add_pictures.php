<?php $this->load->view("admin/header"); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'pictures')
			{
				$(value).attr("class","active");
			}
		});
		$('#ImagePhoto').change(function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			$("#ImagePhotoPreview").fadeIn("fast").attr('src',tmppath);
		});
		
		$('#Image').change(function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			$("#ImagePreview").fadeIn("fast").attr('src',tmppath);
		});
		
		$("#dpd1").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0",
			dateFormat : 'dd-mm-yy',
		});
		
		/*$("#dpd2").timepicker({
			//showOn: "button",
			//buttonImage:image_path,
			//buttonImageOnly: true,
			showSecond:true,
			timeFormat: 'hh:mm:ss',
			//changeMonth: true,
			//changeYear: true,
			//yearRange: "-100:+0",
			//dateFormat : 'dd-mm-yy',
		});*/
		//CKEDITOR.replace('page_content');
	});
</script>
<section class="vbox" id="bjax-el">
	<section class="scrollable wrapper-lg">
	<?php

        	$message = $this->message_stack->message('message');

			if($message != ""){

        ?>

        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">

            <button data-dismiss="alert" class="close" type="button">×</button>

            <i class="fa fa-ok-sign"></i><?php echo $message; ?>

        </div>
        <div class="row"></div>
        <?php } ?>
		<div class="row">
			<section class="panel panel-default col-md-12">
				<header class="panel-heading font-bold"><?php if(!empty($record) && $record['id'] != ""){ echo "Edit"; }else{ echo "Add"; } ?> pictures</header>
				<div class="panel-body">
					<form name="pictures_form" id="pictures_form" class="form-horizontal" data-validate="parsley" action="<?php echo base_url(); ?>pictures/add_post" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php if(!empty($record) && $record['id'] != ""){ echo $record['id']; } ?>" />
						<div class="line line-dashed b-b line-lg pull-in"></div>
						
						<?php $required = $record['id']==0?'data-required="true"':''; ?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="image_name">Name</label>
							<div class="col-sm-4">
								<input type="text" name="image_name" class="form-control" id="image_name" value="<?php echo $record['image_name']; ?>" data-required="true">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Photo</label>
							<div class="col-sm-4">
								<input type="hidden" name="image_path" value="<?php if(!empty($record) && $record['image_path'] != ""){ echo $record['image_path']; } ?>">
								<input type="hidden" name="thumb_path" value="<?php if(!empty($record) && $record['thumb_path'] != ""){ echo $record['thumb_path']; } ?>">
								<input type="hidden" name="720_path" value="<?php if(!empty($record) && $record['720_path'] != ""){ echo $record['720_path']; } ?>">
								<input type="hidden" name="360_path" value="<?php if(!empty($record) && $record['360_path'] != ""){ echo $record['360_path']; } ?>">
								<input name="Image" id="Image" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-4">
							<?php
								$deafultImage = $this->assets->url('photo.jpg','admin');
								if(!empty($record) && $record['thumb_path'] != "")
								{
									$deafultImage = base_url().$record['thumb_path'];
								}
							?>
								<img id="ImagePreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Description</label>
							<div class="col-sm-4">
								<textarea type="textarea" name="description" class="form-control" data-required="true"><?php echo $record['description']; ?></textarea>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Location</label>
							<div class="col-sm-4">
								<input type="text" name="location" class="form-control" id="input-id-1" value="<?php echo $record['location']; ?>" data-required="true">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Photographer</label>
							<div class="col-sm-4">
								<input type="text" name="photographer_name" class="form-control" id="input-id-1" value="<?php echo $record['photographer_name']; ?>" data-required="true">
							</div>
						</div>
						<div style="display:none;" class="form-group">
							<label class="col-sm-2 control-label">Photographer Photo</label>
							<div class="col-sm-4">
								<input type="hidden" name="photographer_photo" value="<?php if(!empty($record) && $record['photographer_photo'] != ""){ echo $record['photographer_photo']; } ?>">
								<input name="ImagePhoto" id="ImagePhoto" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
							</div>
						</div>
						<div style="display:none;" class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-4">
							<?php
								$deafultImage = $this->assets->url('photo.jpg','admin');
								if(!empty($record) && $record['photographer_photo'] != "")
								{
									$deafultImage = base_url().$record['photographer_photo'];
								}
							?>
								<img id="ImagePhotoPreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Photo sorce link</label>
							<div class="col-sm-4">
								<input type="text" name="photo_source_link" class="form-control" id="input-id-1" value="<?php echo $record['photo_source_link']; ?>" data-required="true">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Category</label>
							<div class="col-sm-4">
								<select style="height:300px;" class="form-control m-b parsley-validated" name="category_id[]" multiple="" data-required="true">
								<?php foreach($categories as $category):
									$selected = '';
									if(!empty($record['category_id']))
									{
										$categories = explode(',', $record['category_id']);
										if(in_array($category['id'],$categories))
										{
											$selected = 'selected="selected"';
										}
									}
								?>
									<option <?php echo $selected; ?> value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Copyright</label>
							<div class="col-sm-4">
								<select class="form-control m-b parsley-validated" name="copyright_id" data-required="true">
								<option value="">Please choose</option>
								<?php foreach($copyrights as $copyright): 
									$selected = $copyright['id']==$record['copyright_id']?"selected=selected":"";
								?>
									<option <?php echo $selected; ?> value="<?php echo $copyright['id']; ?>"><?php echo $copyright['copyright_title']; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						
						<!--
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Tags</label>
							<div class="col-sm-4">
								<?php foreach($tags as $tag): 
								$checked = "";
								$tagarray = explode(',',$record['tags']);
								if(in_array($tag['id'], $tagarray)){
									$checked = "checked";
								}
								?>
								<input type="checkbox" name="tags[]" value="<?php echo $tag['id']; ?>" <?php echo $checked; ?> data-required="true"><?php echo $tag['tag']; ?><br>	
								<?php endforeach; ?>
							</div>
						</div>-->
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Tags</label>
							<div class="col-sm-4">
								<input type="textbox" class="form-control" name="tags" value="<?php echo $record['tag_name']; ?>" data-required="true">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Is show?</label>
							<div class="col-sm-4">
							<?php 
							$checkedy = $record['is_show']=='y'?"checked":"";
							$checkedn = $record['is_show']=='n'?"checked":""; ?>
							<input type="radio" name="is_show" value="y" data-required="true" <?php echo $checkedy; ?>>Yes<br>
							<input type="radio" name="is_show" value="n" data-required="true" <?php echo $checkedn; ?>>No<br>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-2">
								<button type="submit" class="btn btn-info">Save</button>
								<button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url(); ?>pictures'">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</section>
</section>
<?php
	$this->assets->load("file-input/bootstrap-filestyle.min.js",'admin');
	echo $this->assets->display_header_assets();
?>
<?php $this->load->view("admin/footer"); ?>