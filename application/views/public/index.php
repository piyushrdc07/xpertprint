<!DOCTYPE html>
<html>
<head>

    <!-- Title -->
    <title>Xpert Print | Professional Custom Business Cards | Design Custom Products at Printing Now</title>

    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Modern Landing Page" />
    <meta name="keywords" content="landing" />
    <meta name="author" content="Steelcoders" />

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,400,300' rel='stylesheet' type='text/css'>
    <?php
    $this->assets->load("../plugins/pace-master/themes/blue/pace-theme-flash.css");
    $this->assets->load("../plugins/uniform/css/uniform.default.min.css");
    $this->assets->load("../plugins/bootstrap/css/bootstrap.min.css");
    $this->assets->load("../plugins/fontawesome/css/font-awesome.css");
    $this->assets->load("../plugins/animate/animate.css");
    $this->assets->load("../plugins/tabstylesinspiration/css/tabs.css");
    $this->assets->load("../plugins/tabstylesinspiration/css/tabstyles.css");
    $this->assets->load("../plugins/pricing-tables/css/style.css");
    $this->assets->load("landing.css");
    $this->assets->load("../plugins/pricing-tables/js/modernizr.js");
    echo $this->assets->display_header_assets();
    ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body data-spy="scroll" data-target="#header">
<nav id="header" class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            <a href="#"><img class="navbar-brand" width="175px" src="<?php echo $this->assets->url("logo.jpg"); ?>" alt="Xpert Print" /></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#home">Home</a></li>
                <li><a href="#features">Features</a></li>
                <li><a href="#aboutus">About Us</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="home" id="home">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="home-text col-md-8">

                <h1 class="wow fadeInDown" data-wow-delay="1.5s" data-wow-duration="1.5s" data-wow-offset="10">Xpert Print</h1>
                <!-- <img width="300px" src="<?php echo $this->assets->url("logo.jpg"); ?>" alt="Xpert Print" /> -->
                <p class="lead wow fadeInDown" data-wow-delay="2s" data-wow-duration="1.5s" data-wow-offset="10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<br>Aenean commodo ligula eget dolor.</p>
            </div>
            <div class="scroller">
                <div class="mouse"><div class="wheel"></div></div>
            </div>
        </div>
    </div>
</div>

<div class="container" id="features">
    <div class="row features-list">
        <div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
            <div class="feature-icon">
                <i class="fa fa-laptop"></i>
            </div>
            <h2>Custom Business Cards</h2>
            <p><strong>Promote Yourself and Your Business</strong></p>
            <p>1000s of professional business card designs printed on premium paper with high-resolution ink. Great cards start at <strike>₹600.00</strike> ₹399.00</p>
            <p><a class="btn btn-link" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">
            <div class="feature-icon">
                <i class="fa fa-lightbulb-o"></i>
            </div>
            <h2>Promotional Products</h2>
            <p><strong>Market Your Business in New Ways</strong></p>
            <p>Hand-picked promotional products perfect for keeping your business on the top of your customers' minds. Design custom products in just minutes.</p>
            <p><a class="btn btn-link" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
            <div class="feature-icon">
                <i class="fa fa-support"></i>
            </div>
            <h2>Company Logo Design</h2>
            <p><strong>Start Branding Your Business</strong></p>
            <p>Design a professional, personalized company logo for your business in minutes. If you love it, make it yours for just <strike>₹5000.00</strike> ₹2490.00.</p>
            <p><a class="btn btn-link" href="#" role="button">View details &raquo;</a></p>
        </div>
    </div>
</div>
<section id="section-1">
    <div class="container" id="aboutus">
        <div class="row">
            <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                <img src="<?php echo $this->assets->url("iphone.png"); ?>" class="iphone-img" alt="">
            </div>
            <div class="col-sm-8 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                <h1>About Us</h1>
                <p>xpertprint.in is the most affordable & hassle free online design & print solution provider company.</p>
                <p>Business cards, Visiting cards, Flyer, Brochure, letterhead, Envelope etc.</p>
                <p>For any help regarding design & print assistance or consultation feel free to get in touch support@xpertprint.in</p>
                <ul class="list-unstyled features-list-2">
                    <li><i class="fa fa-diamond icon-state-success m-r-xs icon-md"></i>Unique design</li>
                    <li><i class="fa fa-check icon-state-success m-r-xs icon-md"></i>Everything you need</li>
                    <li><i class="fa fa-cogs icon-state-success m-r-xs icon-md"></i>Tons of features</li>
                    <li><i class="fa fa-cloud icon-state-success m-r-xs icon-md"></i>Easy to use &amp; customize</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="section-2">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                <section>
                    <div class="tabs tabs-style-linebox">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href=""><span>Design Services</span></a></li>
                                <li class=""><a href=""><span>Brochure</span></a></li>
                                <li class=""><a href=""><span>Flyers & Leaflets</span></a></li>
                                <li class=""><a href=""><span>Stationary</span></a></li>
                                <li class=""><a href=""><span>Customized Gifts</span></a></li>
                            </ul>
                        </nav>
                        <div class="content-wrap">
                            <section class="content-current">
                                <h1>Design Services</h1>
                                <p>Graphic designing is visual communication.
                                    The art of visual representation of ideas through images, symbols, shapes, text and colours.
                                    Designs are all around us as we come across thousands of designs everyday e.g., an attention seeking logo, an impressive business card, a stunning brochure, an engrossing website all these are visual communications that convey messages.
                                    Graphic designing is an art with a purpose, an artists visual representation. Today every company uses these graphic design tools to either sell their products or services or create their own brand. Go ahead hire our experts &amp; make your own brand.</p></section>
                            <section><p>
                                <h1>Brochure</h1>
                                <p>Brochures are indispensable marketing material for any company. We have predesigned editable brochure templates (Browse Design) or custom design your brochure.</p></section>
                            <section><p>
                                <h1>Flyers &amp; Leaflets</h1>
                                <p>Promote Your Business (or) Event &amp; Attract Attention with Multicolour Digital Printed A4 Flyers. Flyers are must for your company to market your product and services. We have predesigned editable flyers templates in (Browse Design) or custom design your flyers &amp; you can upload your design for printing.</p></section>
                            <section><p>
                                <h1>Stationary</h1>
                                <p>Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis.<br>Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam.</p></section>
                            <section><p>
                                <h1>Customized Gifts</h1>
                                <p>Elegant Card Holder With Steel Finish. Protects Your Business Cards From Damages. This Is A Great Gifting Product For Working Professionals With Personalized Laser Engraving.</p></section>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-sm-4 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                <img src="<?php echo $this->assets->url("iphone2.png"); ?>" class="iphone-img" alt="">
            </div>
        </div>
    </div>
</section>
<section id="section-3">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <p class="text-white">“Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero.”</p>
                                    <span>- David, App Manager</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <p class="text-white">“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.”</p>
                                    <span>- Sandra, Director</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <p>“Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit.”</p>
                                    <span>- Amily, UI Designer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 wow rotateInUpLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
                <a href="#contact" class="btn btn-success btn-lg btn-rounded contact-button"><i class="fa fa-envelope-o"></i></a>
                <h2>Let's keep in touch</h2>
                <form class="m-t-md">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control input-lg contact-name" placeholder="Name">
                            </div>
                            <div class="col-sm-6">
                                <input type="email" class="form-control input-lg" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="4=6" placeholder="Message"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default btn-lg">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <p class="text-center no-s"><?php echo date("Y"); ?> &copy; Xpert Print.</p>
    </div>
</footer>

<?php
$this->assets->load("../plugins/jquery/jquery-2.1.4.min.js");
$this->assets->load("../plugins/jquery-ui/jquery-ui.min.js");
$this->assets->load("../plugins/pace-master/pace.min.js");
$this->assets->load("../plugins/bootstrap/js/bootstrap.min.js");
$this->assets->load("../plugins/jquery-slimscroll/jquery.slimscroll.min.js");
$this->assets->load("../plugins/uniform/jquery.uniform.min.js");
$this->assets->load("../plugins/wow/wow.min.js");
$this->assets->load("../plugins/tabstylesinspiration/js/cbpfwtabs.js");
$this->assets->load("../plugins/pricing-tables/js/main.js");
$this->assets->load("landing.js");
echo $this->assets->display_header_assets();
?>

</body>
</html>