<?php
class tags_model extends CI_Model
{
	function insert($data)
	{
		$this->db->insert("tbl_tags", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function update($data)
	{
		try
        {
        	$this->db->update('tbl_tags', $data, array('id' => $data['id']));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }		
	}
	
	function delete($data)
	{
		$this->db->delete("tbl_tags", $data);
        return true;
	}
	
	function get_all()
	{
		$query = $this->db->select("*")->from('tbl_tags')->order_by("id","DESC")->get();
		return $query->result_array();
	}

	function get_by_ids($ids)
	{
		$query = $this->db->select("id,tag")->from('tbl_tags')->where_in('id',$ids)->order_by("id","DESC")->get();
		$result = $query->result_array();
		return $result;
	}
	
	function get_by_id($id)
	{
		$query = $this->db->select("*")->from('tbl_tags')->where(array("id" => $id))->order_by("id","DESC")->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	
}