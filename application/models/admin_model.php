<?php
class Admin_model extends CI_Model
{
	function authenticate($email,$password)
	{
		if($email != "" && $password != ""){
			$this->db->select("AdminID,Email,Name,Username")->from("admin");
			$condition = array(
				"Email" => $email,
				"Password"=>$this->encrypt->sha1($password)
				);
			$this->db->where($condition);
			$query = $this->db->get();
			$admin = $query->result_array();
			if(!empty($admin)){
				return $admin[0];
			}
	        else
	        	return array();
		} else{
			return array();
		}
	}
	
	function authenticateOldPassword($data) {
		$condition = array(
			"AdminID" => $data['AdminID'],
			"Password" => $this->encrypt->sha1($data['oldPassword'])
		);
		$query = $this->db->select('*')->from('admin')->where($condition)->get();
		$admin = $query->result_array();
		if(isset($admin[0])){
			return TRUE;
		}
		return FALSE;
	}
	
	function get_data_by_id($admin_id)
	{
		$query = $this->db->select("*")->from("admin")->where("AdminID", $admin_id)->get();
		return $query->result_array();
	}
	
	/**
	 * Function For Get User Data by email id
	 * @return Array
	 */
	function getUserByEmail($email) {
		$this -> db -> select('AdminID,Email,Name,Username');
		$this -> db -> from('admin');
		$this -> db -> where("Email", $email);
		$query = $this -> db -> get();
		$result = $query -> result_array();
		return $result;
	}
	
	function insert($data)
	{
		$this->db->insert("admin", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function delete($data)
	{
		$this->db->where('AdminID', $data['AdminID'])->delete('admin');
		return true; 
	}
	
	function update($data)
	{
		try
        {
       		$this->db->update('admin',$data, array('AdminID' => $data['AdminID']));
       		return true;
        }
        catch (Exception $e)
        {
            return false;
        }
	}

	/**
	 * Function For Check Is Login Or not
	 * @access public
	 * @param
	 */
	
	function isLogin()
	{
		if($this->session->userdata('AdminID'))
		{
			return true;
		}
		else
		{
			$this->message_stack->add_message('message','Invalid email address or password.');
			$this -> message_stack -> add_message('class', 'danger');
			$url = base_url().'admin';
			redirect($url);
		}
	}

	function get_by_id($admin_id)
	{
		$query = $this->db->select("AdminID, Email, Name, Username, PerPageRecord")->from("admin")->where("AdminID", $admin_id)->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	
	function get_all()
	{
		$query = $this->db->select("AdminID, Email, Name, Username")->from('admin')->where(array("IsDelete"=>'0'))->order_by("CreatedDate","DESC")->get();
		return $query->result_array();
	}
	
	function get_setting($admin_id = 1)
	{
		$query = $this->db->select("Download, Information")->from("admin")->where("AdminID", $admin_id)->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}

	function get_per_page_record()
	{
		$query = $this->db->select("PerPageRecord")->from("admin")->where("AdminID", '1')->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0]['PerPageRecord'];
		}else{
			return '10';
		}
	}
}