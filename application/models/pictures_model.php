<?php
class pictures_model extends CI_Model
{
	function get_download_by_picture_id($picyure_id)
	{
		// wd.downloads as weekly_downloads, md.downloads as monthly_downloads, yd.downloads as yearly_downlods,
		$all_qry = "SELECT COALESCE(tbl_lastweek.lastweek,0) as weekly_downloads, COALESCE(tbl_lastmonth.lastmonth,0) as monthly_downloads, COALESCE(tbl_lastyear.lastyear,0) as yearly_downlods FROM
			(SELECT id,image_name FROM `tbl_picture`) all_pictures
			LEFT JOIN
			(SELECT picture_id, sum(downloads) as lastweek 
			FROM `tbl_today_download`
			where DATE(date) > (NOW() - INTERVAL 7 DAY)
			group by date) tbl_lastweek on all_pictures.id = tbl_lastweek.picture_id
			LEFT JOIN
			(SELECT picture_id, sum(downloads) as lastmonth 
			FROM `tbl_today_download` 
			where DATE(date) > (NOW() - INTERVAL 30 DAY)
			group by date) tbl_lastmonth on all_pictures.id = tbl_lastmonth.picture_id
			LEFT JOIN
			(SELECT picture_id, sum(downloads) as lastyear 
			FROM `tbl_today_download` 
			where DATE(date) > (NOW() - INTERVAL 365 DAY)
			group by date) tbl_lastyear on all_pictures.id = tbl_lastyear.picture_id WHERE all_pictures.id =  {$picyure_id}";
		$resource = $this->db->query($all_qry);
		$result = $resource->result_array();
		return $result[0];
	}

	function get_by_category_id($data)
	{
		$this->db->start_cache();
		$where = "cate.status = 'y' AND find_in_set('".$data['ID']."',pic.category_id)";
		$this->db->select('pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location, cate.id as cat_id, cate.category as cat_name, pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, 0 as today_downloads,copy.copyright_title',false);
		$this->db->from('tbl_categories as cate');
		$this->db->join('tbl_picture as pic','cate.id = pic.category_id','left');
	//	$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->where($where);
		$this->db->order_by('pic.id', 'desc');
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	
	function get_by_tag($data)
	{ 
		$this->db->start_cache();
		$where = "tag.status = 'y' AND find_in_set('".$data['ID']."',pic.tags)";
		$this->db->select('pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, 0 as today_downloads,,copy.copyright_title',false);
		$this->db->from('tbl_picture as pic');
		$this->db->join('tbl_tags as tag','pic.tags = tag.id','left');
	//	$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->where($where);
		$this->db->order_by('pic.upload_date', 'desc');
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	
	function get_by_random($data)
	{ 
		$this->db->start_cache();
		$this->db->select('pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, COALESCE(td.downloads,0) as today_downloads,copy.copyright_title',false);
		$this->db->from('tbl_picture as pic');
		$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->order_by('pic.image_name', 'RANDOM');
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	
	function get_by_recents($data)
	{
		$this->db->start_cache();
		$this->db->select('pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, COALESCE((select downloads from tbl_today_download where picture_id = pic.id and DATE(date) = DATE(NOW())),0) as today_downloads, copy.copyright_title',false);
		$this->db->from('tbl_picture as pic');
		//$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->order_by('pic.id', 'desc');
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	function get_by_download($data)
	{
		$this->db->start_cache();
		$this->db->select("pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, COALESCE((select downloads from tbl_today_download where picture_id = pic.id and DATE(date) = DATE(NOW())),0) as today_downloads, copy.copyright_title",false);
		$this->db->from('tbl_picture as pic');
		//$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->order_by('pic.total_downloads', 'desc');
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	function get_week_download($data)
	{
		$where = 'pic.id IN ( select id from tbl_picture ) AND DATE(date) > (NOW() - INTERVAL 7 DAY)';
		$this->db->start_cache();
		$this->db->select("pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, COALESCE(sum(td.downloads),0) as weekly_downloads, copy.copyright_title",false);
		$this->db->from('tbl_picture as pic');
		$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->where($where);
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->group_by('pic.id');
		$this->db->order_by('weekly_downloads', 'desc');
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	function get_month_download($data)
	{
		$where = 'pic.id IN ( select id from tbl_picture ) AND DATE(date) > (NOW() - INTERVAL 30 DAY)';
		$this->db->start_cache();
		$this->db->select("pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, COALESCE(sum(td.downloads),0) as monthly_downloads, copy.copyright_title",false);
		$this->db->from('tbl_picture as pic');
		$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->where($where);
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->group_by('pic.id');
		$this->db->order_by('monthly_downloads', 'desc');
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	function get_year_download($data)
	{
		$where = 'pic.id IN ( select id from tbl_picture ) AND DATE(date) > (NOW() - INTERVAL 365 DAY)';
		$this->db->start_cache();
		$this->db->select("pic.id, pic.image_name, pic.category_id as categories, pic.tags, pic.image_path, pic.image_width, pic.image_height, pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height, pic.description, pic.location,pic.photographer_name, pic.photographer_photo, pic.photo_source_link, pic.total_downloads, COALESCE(sum(td.downloads),0) as yearly_downlods, copy.copyright_title",false);
		$this->db->from('tbl_picture as pic');
		$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->where($where);
		$this->db->stop_cache();
		$totalRows = $this->db->count_all_results();
		$this->db->group_by('pic.id');
		$this->db->order_by('yearly_downlods', 'desc');
		$this->db->limit($data['Limit'],$data['Offset']);		
		$query = $this->db->get();
		$result['result'] = $query->result_array();
		$this->db->flush_cache();
		$result['pages'] = $totalRows;
		return $result;
	}
	
	function get_picture($data){
	
		$where = 'pic.id = '.$data['ID'];
		$this->db->select('pic.id,pic.image_path,pic.image_name,pic.category_id as categories, pic.tags, pic.image_width,pic.image_height,pic.thumb_path,pic.thumb_width,pic.thumb_height, pic.720_path, pic.720_width, pic.720_height, pic.360_path, pic.360_width, pic.360_height,pic.description,pic.location,pic.photographer_name,pic.photographer_photo,pic.photo_source_link,pic.total_downloads,COALESCE(td.downloads,0) as today_downloads,pic.upload_date,copy.copyright_title',false);
		$this->db->from('tbl_picture as pic');
		$this->db->join('tbl_today_download as td','td.picture_id = pic.id','left');
		$this->db->join('tbl_copyrights as copy','copy.id = pic.copyright_id','left');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query->result_array();
		
	}
	
	function insert($data)
	{
		$this->db->insert("tbl_picture", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function update($data)
	{
		try
        {
			$this->db->update('tbl_picture', $data, array('id' => $data['id']));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }		
	}
	
	function delete($data)
	{
		$this->db->delete("tbl_picture", $data);
        return true;
	}
	
	function get_all()
	{
		$query = $this->db->select("*")->from('tbl_picture')->order_by("id","DESC")->get();
		return $query->result_array();
	}
	
	function get_by_id($id)
	{
		$query = $this->db->select("*")->from('tbl_picture')->where(array("id" => $id))->order_by("id","DESC")->get();
		$result = $query->result_array();
		
		$tags = rtrim($result[0]['tags'],',');
		//print_r($tags);exit;
		$query = $this->db->query('SELECT `tag` FROM (`tbl_tags`) WHERE `id` IN ('.$tags.')');
		
		foreach ($query->result() as $row)
		{
		   $tag_name .= ','.$row->tag;
		}	
		
		$result[0]['tag_name'] =  substr($tag_name,1);
		
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	
	function get_copyright()
	{
		$query = $this->db->select("id,copyright_title")->from('tbl_copyrights')->where(array("status"=>"y"))->order_by("id","DESC")->get();
		return $query->result_array();
	}
	
	function get_category()
	{
		$query = $this->db->select("id,category")->from('tbl_categories')->where(array("status"=>"y"))->order_by("category","ASC")->get();
		return $query->result_array();
	}
	
	function get_tags()
	{
		$query = $this->db->select("id,tag")->from('tbl_tags')->where(array("status"=>"y"))->order_by("id","DESC")->get();
		return $query->result_array();
	}
	
	function check_tags($tag){
		$query = $this->db->select("id")->from('tbl_tags')->where(array("tag"=>$tag))->get();
		$data = $query->result_array();
		return $data[0]['id'];
	}
	
	function add_tags($tag){
		$data['tag'] = $tag;
		$data['tag_date'] = date('Y-m-d h:i:s');
		$this->db->insert("tbl_tags", $data);
        return $this->db->insert_id();
	}
	
	function get_picture_tags($picture_id)
	{	
		$this->db->select("tags");
		$this->db->from('tbl_picture');
		$this->db->where('id = "'.$picture_id.'"');
		$query = $this->db->get();
		$tag_ids =  $query->result_array();
		//print_r($tag_ids);exit;
		$this->db->select("id,tag as tagName");
		$this->db->from('tbl_tags');
		$this->db->where('id IN('.$tag_ids[0]['tags'].')');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
		
		
	}
	
	function get_picture_categories($picture_id)
	{	
		$this->db->select("category_id");
		$this->db->from('tbl_picture');
		$this->db->where('id = "'.$picture_id.'"');
		$query = $this->db->get();
		$cate_ids =  $query->result_array();
		//print_r($cate_ids);exit;
		$this->db->select("id,category as categoryName");
		$this->db->from('tbl_categories');
		$this->db->where('id IN('.$cate_ids[0]['category_id'].')');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	function get_all_ajax($sortBy = '', $sortOrder = '',$search = '',$limit = 10, $offset = 0,$findBy='')
	{
		$sortBy = (empty($sortBy) || $sortBy == "") ? "p.id" : $sortBy;
        $sortOrder = (empty($sortOrder)) ? "desc" : $sortOrder;
		$where = "p.is_show = 'y'";
		if(!empty($search))
		{
			$where .= " AND p.image_name LIKE '%".$search."%'";
		}
		if(!empty($findBy))
		{
			if(!empty($findBy['category_id']))
			{
				$where .= " AND p.category_id REGEXP '[[:<:]]{$findBy['category_id']}[[:>:]]'";
			}
		}
		$this->db->select('p.*');
		$this->db->from('tbl_picture as p');
		$this->db->where($where);
		$this->db->order_by($sortBy,$sortOrder);
		if($limit > 0)
        {
			$this->db->limit($limit,$offset);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}

    function get_by_source_link($link)
    {
        $query = $this->db->select("*")->from('tbl_picture')->where(array("photo_source_link" => $link))->order_by("id","DESC")->get();
        $result = $query->result_array();
        if($result)
        {
            return false;
        }else{
            return true;
        }
    }
}