<?php
class today_download_model extends CI_Model
{
	function check_by_date($date,$pictureID)
	{
		$where = "picture_id = {$pictureID} AND date = '".date('Y-m-d',strtotime($date))."'";
		$query = $this->db->select("*")->from('tbl_today_download')->where($where)->order_by("id","DESC")->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	function insert($data)
	{
		$this->db->insert("tbl_today_download", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function update($data)
	{
		try
        {
        	$this->db->update('tbl_today_download', $data, array('id' => $data['id']));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }		
	}
	
	function delete($data)
	{
		$this->db->delete("tbl_today_download", $data);
        return true;
	}
	
	function get_all()
	{
		$query = $this->db->select("*")->from('tbl_today_download')->order_by("id","DESC")->get();
		return $query->result_array();
	}

	function get_by_ids($ids)
	{
		$query = $this->db->select("tag")->from('tbl_today_download')->where_in('id',$ids)->order_by("id","DESC")->get();
		$result = $query->result_array();
		return $result;
	}
	
	function get_by_id($id)
	{
		$query = $this->db->select("*")->from('tbl_today_download')->where(array("id" => $id))->order_by("id","DESC")->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	
}