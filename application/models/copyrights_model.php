<?php
class copyrights_model extends CI_Model
{
	function insert($data)
	{
		$this->db->insert("tbl_copyrights", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function update($data)
	{
		try
        {
        	$this->db->update('tbl_copyrights', $data, array('id' => $data['id']));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }		
	}
	
	function delete($data)
	{
		$this->db->delete("tbl_copyrights", $data);
        return true;
	}
	
	function get_all()
	{
		$query = $this->db->select("*")->from('tbl_copyrights')->order_by("id","DESC")->get();
		return $query->result_array();
	}
	
	function get_by_id($id)
	{
		$query = $this->db->select("*")->from('tbl_copyrights')->where(array("id" => $id))->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	
}